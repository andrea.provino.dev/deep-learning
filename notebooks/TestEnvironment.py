from trading_utils.StockEnv import StockEnv
import pandas as pd
from tqdm import tqdm

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.optimizers import Adam

from rl.agents import DQNAgent
from rl.policy import BoltzmannGumbelQPolicy
from rl.memory import SequentialMemory

# ------------------------------------- Init ------------------------------------- #
EPISODES = 10
ENV_NAME = 'Env-00'

df = pd.read_csv('trading_utils/data/AMZN_5min_3m.csv')

env = StockEnv(
    data=df[['open', 'high', 'low', 'close']],
    data_window_size = 60
)

# ------------------------------------ Model ------------------------------------- #
def build_model(states, actions):
    model = Sequential()
    # Transform input data into one-dimensional array
    model.add(Flatten(input_shape=(1,60,4)))    
    model.add(Dense(128, activation='relu'))
    model.add(Dense(48, activation='relu'))
    model.add(Dense(24, activation='relu'))
    model.add(Dense(actions, activation='linear'))
    return model

act = env.action_space.n

obs_space = env.observation_space.shape
print("@OBS:", obs_space)
model = build_model(obs_space, act)
model.summary()


def build_agent(model, actions):
    policy = BoltzmannGumbelQPolicy()
    memory = SequentialMemory(limit=80, window_length=1)
    dqn = DQNAgent(
        model=model, 
        memory=memory, 
        policy=policy, 
        nb_actions=actions, 
        nb_steps_warmup=100, 
        target_model_update=1e-2)
    return dqn


dqn = build_agent(model, act)
dqn.compile(Adam(lr=1e-2), metrics=['mae'])
dqn.fit(env, nb_steps=30000, visualize=False, verbose=1)

# After training is done, we save the final weights.
dqn.save_weights('dqn_{}_weights.h5f'.format(ENV_NAME), overwrite=True)

# pbar = tqdm(total=EPISODES)

# for episode in range(1, EPISODES+1):
#     state = env.reset()
#     done = False
#     score = 0 
    
#     while not done:
#         #env.render()
#         action = env.action_space.sample()
#         n_state, reward, done, info = env.step(action)
#         score+=reward
#     print("Episode summary".center(30, '-'))
#     print('Episode: {}'.format(episode))
#     print('Score: {}'.format(score))
#     print('Wallet: {}'.format(info.wallet))
#     print("End".center(30, '-'))
#     print('\n')
#     pbar.update(1)
# pbar.close()