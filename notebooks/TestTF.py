from trading_utils.TensorflowEnv import Trading
from tf_agents.environments import utils
from enum import Enum
import tensorflow as tf
from tf_agents.environments import tf_py_environment
import numpy as np

class Actions(Enum):
    Buy = 0
    Sell = 1
    Hold = 2




rewards = []
steps = []
num_episodes = 1000

# ----------------------------- Test 1: All actions ------------------------------ #

def TestOne():
    env = tf_py_environment.TFPyEnvironment( Trading())
    cumulative_reward = 0
    # 1 - BUY | 4x - HOLD | 1 - SELL
    time_step = env.step(0)
    cumulative_reward += time_step.reward

    for _ in range(3):
        time_step = env.step(2)
        cumulative_reward += time_step.reward

        time_step = env.step(1)
        cumulative_reward += time_step.reward
    print('Final Reward = ', cumulative_reward)
    
# ---------------------------- Test 2: Random actions ---------------------------- #

def TestTwo():
    env = tf_py_environment.TFPyEnvironment( Trading())
    
    for _ in range(num_episodes):
        episode_reward = 0
        episode_steps = 0
        time_step = env.reset()
        while not time_step.is_last():
            action = tf.random.uniform(
                shape=[1], 
                minval=0, 
                maxval=3, 
                dtype=tf.int32)
            time_step = env.step(action)
            episode_steps += 1
            episode_reward += time_step.reward.numpy()
        rewards.append(episode_reward)
        steps.append(episode_steps)
        time_step = env.reset()
    
TestTwo()
num_steps = np.sum(steps)
avg_length = np.mean(steps)
avg_reward = np.mean(rewards)
print(f"Steps: {num_steps} \n Avg Length: {avg_length} \n Avg Reward: {avg_reward}")