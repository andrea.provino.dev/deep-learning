"""A class to describe the shape and dtype of numpy arrays."""

from gym import Env
from gym.spaces import Discrete, Box
from enum import Enum
import numpy as np
import pandas as pd

from tf_agents.specs import array_spec

# -------------------------------------------- Parameters for the Environment ------------------------------------------- #
MAX_SHARES_PER_BUY = 2                      # Max number of shares to buy

# ----------------------------------------------------------------------------------------------------------------------- #


# describes a numpy array or scalar shape and dtype
# from tf_agents.specs import array_spec


class Actions(Enum):
    Buy = 0
    Sell = 1
    Hold = 2

"""
Obs:
- 2h time frame (40 rows of 5m data)
- OHLC data + Volume
- Portfolio Gain (%)
- (v2) Signals 
"""
class StockEnv(Env):
    def __init__(self, data = pd.DataFrame(), data_window_size = 40):
        """Initialize the Stock environment
        
        Params
        ======
        
            data (Pandas Dataframe): dataframe containing OHLC data
            data_window_size (int): reading buffer
        
        """
        # ------------------------------------- Init ------------------------------------- #
        
        self.action_space = Discrete(3) 
        
        self.observation_space = array_spec.BoundedArraySpec(
            shape=(4,40), dtype=np.float32, minimum=None, maximum=None, name=None
        )

       
        # Buy - Sell
        self.number_trades = 0 
        self.position_open = False
        self.state = data

        
        # ------------------------------------ Trades ------------------------------------ #
        self.history = []
        self.current_position = {}
        self.is_position_open = False
        self.wallet = 0

        # -------------------------------- Reading window -------------------------------- #
        self.reading_window_size = data_window_size
        self.reading_window_step = 0
        self.reading_window_lower_limit = 0
        self.reading_window_upper_limit = data_window_size
        self.reading_window_data = np.array([])
        

    def step(self, action):
        
        # Create a class that will be used to keep track of information about the transaction
        class Info(object):
            # workaround to make training happen with keras-rl
            def items(self):
                return {}
            pass
        info = Info()
        
        reward = 0
        
        self.reading_window_lower_limit = self.reading_window_step
        self.reading_window_upper_limit = self.reading_window_size + self.reading_window_step
        
        self.reading_window_data = self.state.iloc[self.reading_window_lower_limit:self.reading_window_upper_limit]
        
        # ---------------------------- Reward Function ---------------------------- #
        """ 
        - Only on Buy at the time
        - Negative rewards if
        - - Negative trades
        - - Two consecutive Buys / Sells
        - - Buy at lower price after bought at higher
        - - Sell position not closed in trading day
        - - No trades done in given day
        - - Day close negative (%)
        - Positive rewards if
        - - Positive trades
        - - Number of trades > 0
        - - Day close positive (%)
        - - Consistent (Previuous trades higher) position_history
        """
        done = False

        # placeholder for info
        info.reading_step = self.reading_window_step
        info.position = self.current_position
        info.wallet = self.wallet
        
        
        # increment step in our environment 
        self.reading_window_step += 1
        
        # Environment limit
        if(self.reading_window_step == (self.state.shape[0] - self.reading_window_size) - 1):
            done = True
        
        if(action == Actions.Buy.value):
            if(self.is_position_open):
                # position already opened, cannot buy a new one
                # agent failed 
                done = True
            else:
                # Open a new position
                self.is_position_open = True
            
                new_position = self.reading_window_data.iloc[-1]
                
                self.history.append(
                    new_position
                )
                reward = 10
                self.current_position = new_position
        
        if(action == Actions.Sell.value):
            # Sell
            if(self.is_position_open):
                # sell the stock, register in trading history
                
                aval_position = self.reading_window_data.iloc[-1]
                
                aval_price = aval_position['close']
                
                current_price = self.current_position['close']
                
                delta_price = aval_price - current_price
                 
                self.wallet += delta_price
                
                reward = 20
                reward += delta_price
              
            else:
                # no position is open
                reward = -10
            pass
        
        return self.reading_window_data, reward, done, info

    def render(self):
        # Implement vizualization
        # TODO
        pass
    
    def reset(self):
        # Reset Env
        self.history = []
        self.wallet = 0
        self.current_position = {}
        self.reading_window_step = 0
        self.reading_window_lower_limit = 0
        self.reading_window_upper_limit = self.reading_window_size
        self.is_position_open = False
        self.reading_window_data = self.state.iloc[self.reading_window_lower_limit:self.reading_window_upper_limit]
        return self.reading_window_data
