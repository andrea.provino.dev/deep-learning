# --------------------------- Tensorflow Custom Env ----------------------------- #

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc
import tensorflow as tf
import numpy as np
import pandas as pd

from tf_agents.environments import py_environment
from tf_agents.environments import tf_environment
from tf_agents.environments import tf_py_environment

from tf_agents.specs import array_spec
from tf_agents.environments import wrappers
from tf_agents.environments import suite_gym
from tf_agents.trajectories import time_step as ts

def column(matrix, i):
    return [row[i] for row in matrix]

# --------------------------------- Trading Env ---------------------------------- #
class Trading(py_environment.PyEnvironment):

    def __init__(self, look_back_window=40):
        
        self._look_back_window = look_back_window
        
        # ---------------------- Internal state ---------------------- #
        self._action_spec = array_spec.BoundedArraySpec(
            shape=(), dtype=np.int32, minimum=0, maximum=2, name='action'
        )
        # 40 rows of data for 5 columns TODO: check validity
        self._observation_spec = array_spec.BoundedArraySpec(
            shape=(5,self._look_back_window), dtype=np.float32, minimum=0, name='observation')
        self._state = np.zeros((5,self._look_back_window), dtype=np.float32)
        self._episode_ended = False
       
        # ------------------------ Env state ------------------------- #
        self._wallet = 0
        self.fee = 0.05
        self._current_step = 0
        self._buy_cost = 0
        self._entry_points = 0 
        self._hold_counts = 0
        self._risk_averse = .8

        # ------------------------ Load Data ------------------------- #
        self._stock_dataset  = pd.read_csv('./trading_utils/data/AMZN_5min_batch_1.csv') 
        self._stock_dataset = self._stock_dataset[:1000]
        self._state = np.zeros((5,self._look_back_window), dtype=np.float32)
       
    
    # --------------------------- Custom Methods --------------------------- #
    def _reset(self):
        """Return initial_time_step."""
        self._state = np.zeros((5,self._look_back_window), dtype=np.float32)
        self._episode_ended = False
        self._buy_cost = 0
        self._entry_points = 0
        self._hold_counts = 0
        self._current_step = 0
        self._wallet = 0
        return ts.restart( np.array(self._state, dtype=np.float32))
    
    def _step(self, action):
        
        if self._episode_ended:
            # The last action ended the episode. Ignore the current action and start a new episode
            return self.reset()
        
        """Apply action and return new time_step."""
        reward = 0
        lower_limit = self._current_step
        upper_limit = lower_limit + self._look_back_window
        
        _reading_window = self._stock_dataset.iloc[lower_limit:upper_limit]
        
        
        # -------------------- Check end of data --------------------- #
        # TODO: if we get there, you deserve a prize my dear
        # Unless you have to study harder in order to understand how things work
        datapoints, _ = _reading_window.shape
        
        if(datapoints < self._look_back_window):
            # TODO: check if pass previous state is optimal
            # wallet_balance = " €: %d - N: %d - S: %d" % (self._wallet, self._entry_points, self._current_step)
            # print(wallet_balance.center(30, "-"))
            return ts.termination(np.array(self._state, dtype=np.float32), reward)
        
        
        # Get the first samples
        self._state = np.array(
            [
                _reading_window['volume'],
                _reading_window['open'],
                _reading_window['high'],
                _reading_window['low'],
                _reading_window['close']
            ]
        )
        
        
        # new step from state range(self.current_step, self.current_step+self.look_back_window)
        # ---------------------- Handle Actions ---------------------- #
        if action == 0:
            if self._buy_cost == 0:
                # Buy a new stock
                last_obs = column(self._state, -1)
                #self._buy_history.append(last_obs)
                self._buy_cost = last_obs[4]
                self._entry_points =+1
                reward += 1
                reward += self._entry_points
                self._hold_counts = 0
            else:
                # already in trade
                # Try to BUY but already bought
                reward -= 1
                self._episode_ended = True
               
        if action == 1:
            # SELL
            if self._buy_cost == 0:
                # Try to SELL but no stock availables
                reward -= 1
                self._episode_ended = True
            else:
                # Sell the stock
                buy_price = self._buy_cost
                
                last_obs = column(self._state, -1)
                sell_price = last_obs[4]  
                
                delta_price = sell_price - buy_price
                self._wallet += delta_price
                
                reward += delta_price
                if(reward < 0):
                    reward *= (1. + self._risk_averse) 
                self._buy_cost = 0
                self._hold_counts = 0
        if action == 2:
            # 24 are 2h (5minutes each)
            if self._hold_counts == 24:
                self._hold_counts = 0
                reward -= 2
            else:
                self._hold_counts += 1
        
        actions = ['BUY', 'SELL', 'HOLD']
        
        #print(actions[action], self._hold_counts, reward, self._episode_ended)
        # ----------------------- Update State ----------------------- #
        self._current_step += 1
        
        # ----------------------- End Episode ------------------------ #
        if self._episode_ended:
            #self._reward  = 10
            wallet_balance = " €: %d - N: %d - S: %d" % (self._wallet, self._entry_points, self._current_step)
            print(wallet_balance.center(30, "-"))
            if(reward > 3):
                reward += 10
            reward +=  self._current_step

            return ts.termination(np.array(self._state, dtype=np.float32), reward)
        else:
            return ts.transition(np.array(self._state, dtype=np.float32), reward,discount=0.9)
    
    
    # -------------------------- Default methods --------------------------- #


    def observation_spec(self):
        """Return observation_spec."""
        return self._observation_spec
    
    def action_spec(self):
        """Return action_spec."""
        return self._action_spec